const jwt = require('jsonwebtoken')

const verification = function (req, res) {
    if (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]) {
            return res.status(422).json({ status: "ok", "status code": 422, response: { Message: "Please provide the token." } })
    }else{
        const theToken = req.headers.authorization.split(' ')[1];
        return decoded = jwt.verify(theToken, 'the-secret-verification-code');
    }

   
}

const merchant = function (req,res){
    if (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]) {
        return res.status(422).send("Message: Please provide the token.")
    }
    const theToken = req.headers.authorization.split(' ')[1];
    return decoded = jwt.verify(theToken, 'the-secret-login-code');
}

module.exports = {
    verification,
    merchant
}