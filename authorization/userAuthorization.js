const jwt = require('jsonwebtoken')

const email_verify = function (req, res) {
    if (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]) {
        return res.status(422).json({ status: "ok", "status code": 422, response: { Message: "Please provide the token." } })
    } else {
        const theToken = req.headers.authorization.split(' ')[1];
        return decoded = jwt.verify(theToken, 'the-secret-code');
    }
}

const register_user = function (req, res) {
    if (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]) {
        return res.status(422).json({ status: "ok", "status code": 422, response: { Message: "Please provide the token." } })
    } else {
        const theToken = req.headers.authorization.split(' ')[1];
        return decoded = jwt.verify(theToken, 'the-secret-registration-code')
    }


}

const user_details = function (req, res) {
    if (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]) {
        return res.status(422).json({ status: "ok", "status code": 422, response: { Message: "Please provide the token." } })
    } else {

        const theToken = req.headers.authorization.split(' ')[1];
        return decoded = jwt.verify(theToken, 'the-secret-login-code')

    }
}

const user = function (req, res) {
    if (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]) {
        return res.status(422).json({ status: "ok", "status code": 422, response: { Message: "Please provide the token." } })
    } else {
        const theToken = req.headers.authorization.split(' ')[1];
        return decoded = jwt.verify(theToken, 'the-secret-login-code')
    }

}

module.exports = {
    email_verify,
    register_user,
    user_details,
    user
}