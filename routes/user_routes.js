const { userLogin } = require('../controller/admin/login');
const { all_product } = require('../controller/admin/showProduct');
const { validateMerchant } = require('../controller/admin/validateMerchant');
const { addToCart } = require('../controller/user/addToCart');
const { buy } = require('../controller/user/buyProduct');
const { emailsender } = require('../controller/user/emailSender');
const { emailVerification } = require('../controller/user/emailVerification');
const { getUser } = require('../controller/user/getUser');
const { ordered_list } = require('../controller/user/orderedProduct');
const { razorpay, razorverify } = require('../controller/user/razorpay');
const { registerUser } = require('../controller/user/registerUser');
const { showCart } = require('../controller/user/showCart');
const { stripe } = require('../controller/user/stripe');
const { userDetails } = require('../controller/user/userDetails');
const userValidationResult = require('../validation/userValidation');

const router = require('express').Router();


router.post('/email-register', userValidationResult.email_sender, emailsender)
router.post('/verification', userValidationResult.verification, emailVerification)
router.post('/registration', userValidationResult.registration, registerUser)
router.post('/details', userValidationResult.details, userDetails)
router.get('/get', getUser)
router.post('/login', userLogin)
router.post('/add-to-cart', addToCart)
router.get('/show-cart', showCart)
router.post('/buy', buy)
router.get('/show-product', all_product)
router.post('/validate', validateMerchant)
router.get("/order-list", ordered_list)

///////////////////////////////////////////


router.get('/payment', (req, res) => {
    res.sendFile("/home/user/Desktop/sequelize_2/frontend/index.html");
})
router.post('/stripe-pay', stripe)
router.post('/create/order',razorpay)
router.post('/payment/verify', razorverify);
//////////////////////////////////////////

module.exports = { router }