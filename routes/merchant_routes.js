const { merchantLogin } = require('../controller/admin/login');
const { all_product } = require('../controller/admin/showProduct');
const { addProduct } = require('../controller/merchant/addProduct');
const { addStock } = require('../controller/merchant/addStock');
const { getMerchant } = require('../controller/merchant/getMerchant');
const { merchantVerification } = require('../controller/merchant/merchantVerification');
const { registerMerchant } = require('../controller/merchant/registerMerchant');
const merchantValidationResult = require('../validation/merchantValidation');

const router = require('express').Router();

router.post('/register',merchantValidationResult.registration ,registerMerchant )
router.post('/verification',merchantValidationResult.verification, merchantVerification)
router.post('/add-product',addProduct)
router.put('/update-product', addStock)
router.get('/get', getMerchant)
router.post('/login', merchantLogin)
router.get('/show-product',all_product)

module.exports = {router}