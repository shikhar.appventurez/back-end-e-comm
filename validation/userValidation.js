var bodyParser = require("body-parser");

const router = require("express").Router();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var Joi = require("joi");


const userValidationResult = {
  email_sender: (req, res, next) => {
    const schema = Joi.object({
      email: Joi.string().lowercase().email().required().error(new Error("Email is required."))
  })
    errorHandler(schema, next, req, res)
  },
  verification: (req, res, next) => {
    const schema = Joi.object({
      otp: Joi.number().required().error(new Error("OTP is required."))
    })
    errorHandler(schema, next, req, res)
  },
  registration: (req, res, next) => {
    const schema = Joi.object({
      fullname: Joi.string().required().trim().error(new Error("FullName is required.")),
      email: Joi.string().lowercase().email().trim().required().error(new Error("Email is required.")),
      phoneNo: Joi.string().length(10).pattern(/^[0-9]+$/).required().error(new Error("Phone number is required")),
      password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).error(new Error("Password is required.")),
    })
    errorHandler(schema, next, req, res)
  },
  details: (req,res,next) => {
    const schema = Joi.object({
      address: Joi.string().trim().required().error(new Error("Address is required."))
    })
    errorHandler(schema,next,res,req);
  }
}

function errorHandler(schema, next, req, res) {
  const {error}  = schema.validate(req.body, { abortEarly: false });
  if (error) {
    res.status(422).json({status: 422,error: error.message});
  } else {
    next();
  }
}
module.exports = userValidationResult;
