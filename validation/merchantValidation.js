var bodyParser = require("body-parser");

const router = require("express").Router();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var Joi = require("joi");


const merchantValidationResult = {


    registration: (req, res, next) => {
        const schema = Joi.object({
            fullname: Joi.string().required().trim().error(new Error("FullName is required.")),
            email: Joi.string().lowercase().email().trim().required().error(new Error("Email is required.")),
            phoneNo: Joi.string().length(10).required().error(new Error("Phone number is required")),
            address: Joi.string().trim().required().error(new Error("Address is required."))
        })
        errorHandler(schema, next, req, res)
    },

    verification: (req, res, next) => {
        const schema = Joi.object({
            password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).error(new Error("Password is required.")),
        })
        errorHandler(schema, next, req, res)
    }
}

function errorHandler(schema, next, req, res) {
    const { error } = schema.validate(req.body, { abortEarly: false });
    if (error) {
        res.status(422).json({ status: 422, error: error.message });
    } else {
        next();
    }
}
module.exports = merchantValidationResult;
