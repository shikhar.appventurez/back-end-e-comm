const Category = require("../models/category");
const Merchant = require("../models/merchant");
const Product = require("../models/product");
const Subcategory = require("../models/subcategory");
const Orderproduct = require("../models/order_product");
const { user } = require("../authorization/userAuthorization");


const all_product = async function (category, subcategory, product, merchant, page, limit) {
    return await Product.findAll({
        include: [{
            model: Category,
            as: 'category',
            attributes: ['name'],
            where: { name: category }
        }, {
            model: Subcategory,
            as: 'subcategory',
            attributes: ['name'],
            where: { name: subcategory }
        }, {
            model: Merchant,
            as: 'merchant',
            attributes: ['fullname'],
            where: { fullname: merchant }
        }],
        where: { product_name: product },
        attributes: { exclude: ['subcategory_id', 'Category_ID', 'createdAt', 'updatedAt', 'merchant_id'] },
        offset: (page - 1) * limit,
        limit: +limit
    }
    );

}


const find_by_user = async function (id) {
    return await Orderproduct.findOne({ where: { user_id: "a9dfe29d-d902-4c6c-a8f1-d6d593a7e1f3" } })
}

const order_list = async function (id) {
    return await Orderproduct.findAll({ where: { user_id: id }, attributes: { exclude: ['createdAt', 'updatedAt', 'user_id', 'product_id',] } })
}



module.exports = {
    all_product,
    order_list,
    find_by_user

}