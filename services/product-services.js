const { Sequelize } = require('sequelize');
const Cart = require('../models/cart');
const Category = require('../models/category');
const Merchant = require('../models/merchant');
const Product = require('../models/product');
const Subcategory = require('../models/subcategory');


const add_category = async function (category_name) {
    return await Category.findOrCreate({ where: { name: category_name } })
}

const add_sub_category = async function (subcategory_name, category) {
    return await Subcategory.findOrCreate({ where: { name: subcategory_name, Category_ID: category.id } })
}

const add_product = async function (info, product_name, category, subcategory, id) {
    return await Product.findOrCreate({ where: { product_name: product_name, Category_ID: category.id, subcategory_id: subcategory.id, merchant_id: id }, defaults: info })
}

const find_product = async function (id) {
    return await Product.findOne({ where: { id: id } })
}

const add_stock = async function (info, id) {
    return await Product.update(info, { where: { id: id } })
}

const add_to_cart = async function (info, product, id) {
    return await Cart.findOrCreate({ where: { product_id: product.id, user_id: id }, defaults: info })
}

const add_quantity = async function (quantity, totalprice, cartProduct) {
    return await Cart.update({ quantity: quantity, totalprice: totalprice }, { where: { id: cartProduct.id } })
}

const cart_product = async function (id) {
    return await Product.findOne({
        include:[{
            model: Merchant,
            attributes: ['fullname'],
        }],
        where: { id: id },
        attributes: { exclude: ["stock", "merchant_id", "Category_ID", "subcategory_id", "createdAt", "updatedAt"] },
        
    })
}

const order_product = async function (id) {
    return await Product.findAll({
        where: { id: id },
        order:Sequelize.literal('id ASC'),
        attributes: { exclude: ["merchant_id", "Category_ID", "subcategory_id", "createdAt", "updatedAt", "discount", 'id'] },
        raw: true
    })
}

const decrease_stock = async function (product) {
    return await Product.bulkCreate(product, { updateOnDuplicate: ["stock"] })
}

const find_all_product = async function (id) {
    return await Product.findAll({ where: { id: id }, raw: true })
}



module.exports = {
    add_category,
    add_sub_category,
    add_product,
    find_product,
    add_stock,
    add_to_cart,
    add_quantity,
    cart_product,
    order_product,
    decrease_stock,
    find_all_product
}