const Otp = require("../models/otp_verification");
const User = require("../models/user");
const { Op, Sequelize } = require('sequelize');
const Cart = require("../models/cart");
const Orderproduct = require("../models/order_product");
const Orderid = require("../models/order_id");

const updateOrCreate = async function (email, data) {
    const row = await Otp.findOne({ where: { email: email }, raw: true });
    if (row !== null) {
        await Otp.update(data, { where: { email: email } })
    } else {
        await Otp.create(data)
    }
}

const find = async function (email) {
    const row = await User.findOne({ where: { email: email } })
    return row
}

const otpVerify = async function (decoded) {
    const row = await Otp.findOne({
        where: { email: decoded.email },
        raw: true
    })
    return row
}

const create = async function (info) {
    const row = User.create(info)
    return row
}

const update = async function (info, id) {
    const row = await User.update(info, { where: { id: id } });
    return row
}

const admin_get_user = async function (page, limit) {
    return row = await User.findAll({
        where: { usertype: "user" },
        attributes: { exclude: ['password'] },
        offset: (page - 1) * limit,
        limit: +limit
    })
}

const get_user = async function (id) {
    return row = await User.findOne({
        where: { id: id },
        attributes: { exclude: ['password'] }
    });
}

const user_login = async function (email_or_phone) {
    const row = await User.findOne({
        where: {
            [Op.or]: [{ email: email_or_phone },
            { phoneNo: email_or_phone }]
        }
    });
    return row
}

const find_by_id = async function (id) {
    return await User.findOne({ where: { id: id } })
}

const user_cart = async function (id) {
    return await Cart.findAll({ where: { user_id: id }, order: Sequelize.literal('product_id ASC') })
}

const ordered_product = async function (product) {
    return await Orderproduct.bulkCreate(product)
}

const order_id = async function (total_price, decoded) {
    return await Orderid.create({ total_price: total_price, user_id: decoded.id })
}

const empty_cart = async function (id) {
    return await Cart.destroy({ where: { user_id: id } })
}

const order_update = async function (total_price, orderid) {
    return await Orderid.update({ total_price: total_price }, { where: { id: orderid.id } })
}


const find_cart_product = async function (user_id, product_id) {
    return await Cart.findOne({ where: { [Op.and]: [{ user_id: user_id, product_id: product_id }] }, raw: true })
}
module.exports = {
    updateOrCreate,
    find,
    otpVerify,
    create,
    update,
    admin_get_user,
    get_user,
    user_login,
    find_by_id,
    user_cart,
    ordered_product,
    order_id,
    empty_cart,
    order_update,
    find_cart_product
}

