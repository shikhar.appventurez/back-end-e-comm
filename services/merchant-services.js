const Merchant = require("../models/merchant");
const { Op } = require('sequelize');

const find = async function (email) {
    const row = await Merchant.findOne({
        where: { email: email },
        raw: true
    })
    return row

}

const update_or_create = async function (email, data) {
    const row = await Merchant.findOne({ where: { email: email }, raw: true });
    if (row !== null && row.status == 'not-verified') {
        return await Merchant.update(data, { where: { email: email } })
    } else {
        return await Merchant.create(data)
    }
}

const find_by_id = async function (id) {
    return await Merchant.findOne({ where: { id: id }, raw: true })
}

const update_by_id = async function (info, id) {
    return await Merchant.update(info, { where: { id: id }, raw: true })
}

const admin_get_merchant = async function (page, limit, status) {
    const row = await Merchant.findAll({
        where: { usertype: "merchant", status: status },
        attributes: { exclude: ['password'] },
        offset: (page - 1) * limit,
        limit: +limit,
        raw: true
    });
    return row
}

const get_merchant = async function (id) {
    const row = await Merchant.findOne({
        where: { id: id },
        attributes: { exclude: ['password'] },
        raw: true
    });
    return row
}

const merchant_login = async function (email_or_phone) {
    const row = await Merchant.findOne({
        where: {
            [Op.or]: [{ email: email_or_phone },
            { phoneNo: email_or_phone }]
        },
        raw: true
    });
    return row
}

const status_update = async function (status, id) {
    return await Merchant.update({ status: status }, { where: { id: id }, raw: true })
}

module.exports = {
    find,
    update_or_create,
    find_by_id,
    update_by_id,
    admin_get_merchant,
    get_merchant,
    merchant_login,
    status_update
}