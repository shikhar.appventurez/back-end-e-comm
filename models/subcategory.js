const Sequelize = require('sequelize');
const sequelize = require('../utils/database');
const Product = require('./product');

const Subcategory = sequelize.define('subcategory', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    name: { type: Sequelize.STRING, allowNull: false },
})


Subcategory.hasMany(Product, {foreignKey: "subcategory_id", as: "product"});
Product.belongsTo(Subcategory, {foreignKey: "subcategory_id",as : "subcategory"})

module.exports = Subcategory