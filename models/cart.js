const Sequelize = require('sequelize');
const sequelize = require('../utils/database');
const Product = require('./product');
const User = require('./user');



const Cart= sequelize.define('cart', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    quantity:{type: Sequelize.INTEGER, allowNull:false},
    totalprice:{type: Sequelize.INTEGER, allowNull:false}
    
})

Product.hasMany(Cart, {foreignKey: "product_id", as: 'cart'});
Cart.belongsTo(Product, {foreignKey: "product_id",as: 'product'});

User.hasMany(Cart, {foreignKey: "user_id", as: "cart"});
Cart.belongsTo(User, {foreignKey: "user_id", as: "user"});

module.exports = Cart