const Sequelize = require('sequelize');
const sequelize = require('../utils/database')

const User = sequelize.define('users', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    fullname: { type: Sequelize.STRING, allowNull: false },
    email:    { type: Sequelize.STRING, allowNull: false },
    phoneNo : { type: Sequelize.STRING, allowNull: false },
    password: { type: Sequelize.STRING, allowNull: false },
    usertype: { type: Sequelize.STRING, allowNull: false },
    status:   { type: Sequelize.STRING, allowNull: false, defaultValue: "not-verified" },
    address:  { type: Sequelize.STRING, allowNull: true },
    step:     { type: Sequelize.INTEGER, allowNull: false, defaultValue: 0 },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,

});



module.exports = User;