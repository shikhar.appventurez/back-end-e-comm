const Sequelize = require('sequelize');
const sequelize = require('../utils/database');
const Orderid = require('./order_id');
const Product = require('./product');
const User = require('./user');


const Orderproduct = sequelize.define('orderProduct', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    product_name: { type: Sequelize.STRING, allowNull: false },
    quantity: { type: Sequelize.INTEGER, allowNull: false },
    price: { type: Sequelize.INTEGER, allowNull: false },
    total_price: { type: Sequelize.INTEGER, allowNull: false },
    address: { type: Sequelize.STRING, allowNull: false },
    

})

User.hasMany(Orderproduct, {foreignKey: 'user_id'})
Orderproduct.belongsTo(User, {foreignKey: 'user_id'})

Product.hasMany(Orderproduct, {foreignKey: 'product_id'})
Orderproduct.belongsTo(Product, {foreignKey: 'product_id'})

Orderid.hasMany(Orderproduct, {foreignKey: "order_id"})
Orderproduct.belongsTo(Orderid, {foreignKey: 'order_id'})

module.exports = Orderproduct