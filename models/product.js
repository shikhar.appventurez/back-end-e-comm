const Sequelize = require('sequelize');
const sequelize = require('../utils/database')


const Product = sequelize.define('product', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    product_name: { type: Sequelize.STRING, allowNull: false },
    stock: { type: Sequelize.INTEGER, allowNull: true },
    price: { type: Sequelize.INTEGER, allowNull: true },
    discount: { type: Sequelize.INTEGER, allowNull: true },
    discounted_price: { type: Sequelize.INTEGER, allowNull: true },
})


module.exports = Product