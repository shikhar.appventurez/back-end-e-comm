const Sequelize = require('sequelize');
const sequelize = require('../utils/database');
const Product = require('./product');
const Subcategory = require('./subcategory')

const Category = sequelize.define('category', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    name: { type: Sequelize.STRING, allowNull: false },
})

Category.hasMany(Subcategory, {foreignKey: "Category_ID", as: 'subcategory'});
Subcategory.belongsTo(Category, {foreignKey: "Category_ID",as: 'category'});

Category.hasMany(Product, {foreignKey: "Category_ID", as: "product"});
Product.belongsTo(Category, {foreignKey: "Category_ID", as: "category"});

module.exports = Category