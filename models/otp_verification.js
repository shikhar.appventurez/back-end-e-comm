const Sequelize = require('sequelize');
const sequelize = require('../utils/database')

const Otp = sequelize.define('OTP', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    email: { type: Sequelize.STRING, allowNull: false },
    otp: { type: Sequelize.INTEGER, allowNull: false },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.TIME
})

module.exports = Otp