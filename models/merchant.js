const Sequelize = require('sequelize')
const sequelize = require('../utils/database');
const Product = require('./product');


const Merchant = sequelize.define('merchant', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    fullname: { type: Sequelize.STRING, allowNull: false },
    email: { type: Sequelize.STRING, allowNull: false },
    phoneNo: { type: Sequelize.STRING, allowNull: false },
    password: { type: Sequelize.STRING, allowNull: true },
    usertype: { type: Sequelize.STRING, allowNull: false },
    status: { type: Sequelize.STRING, allowNull: false, defaultValue: "not-verified" },
    address: { type: Sequelize.STRING, allowNull: false },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,

});

Merchant.hasMany(Product, { foreignKey: "merchant_id" });
Product.belongsTo(Merchant, { foreignKey: "merchant_id" });

module.exports = Merchant;