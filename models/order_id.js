const Sequelize = require('sequelize');
const sequelize = require('../utils/database');
const User = require('./user');

const Orderid = sequelize.define('orderid', {
    id: { type: Sequelize.UUID, defaultValue:Sequelize.UUIDV4, primaryKey: true, },
    total_price: { type: Sequelize.INTEGER, allowNull: false },
})


User.hasMany(Orderid, {foreignKey: 'user_id'})
Orderid.belongsTo(User, {foreignKey: 'user_id'})


module.exports = Orderid