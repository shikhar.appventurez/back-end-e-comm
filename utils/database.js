const Sequelize = require('sequelize')
const dotenv = require('dotenv').config()

const sequelize = new Sequelize(
    process.env.database,
    process.env.host,
    process.env.password,
    {
        dialect: 'mysql',
        host: 'localhost',
        logging: false
    }
);

module.exports = sequelize