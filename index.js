const express = require('express');
const app = express();
const dotenv = require('dotenv').config()
port = process.env.port
app.use(express.json());

const user_router = require('./routes/user_routes');
const merchant_router = require('./routes/merchant_routes');

const stripe = require("stripe")(process.env.STRIPE_PRIVATE_KEY)


app.use ("/user", user_router.router)
app.use ('/merchant',merchant_router.router )
app.use((err, req, res, next) => {
    err.statusCode = err.statusCode || 500;
    err.message = err.message || "Internal Server Error!";
    res.status(err.statusCode).json("Message: " + err.message)
})



app.listen(port, () => console.log(`Server is running on port ${port}`))