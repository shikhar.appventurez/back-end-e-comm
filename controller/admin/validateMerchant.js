const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");
const { admin } = require('../../authorization/adminAuthorization');
const { find_by_id, status_update } = require('../../services/merchant-services');

exports.validateMerchant = async (req, res, next) => {

    try {
        const decoded = admin(req, res)
        if (decoded.usertype == "admin") {
            const id = req.body.id;
            const status = req.body.status;
            const thetoken = jwt.sign({ id: id }, 'the-secret-verification-code', { expiresIn: '1h' })
            if (status == "verified") {
                await status_update(status, id)
                const row = await find_by_id(id)
                
                const email = row.email;
                const text = `Here is your set up password token: ${thetoken}`
                const verified = async function () {
                    const transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: { user: 'shikhar@appventurez.com', pass: process.env.pass }

                    })

                    const info = {
                        from: 'shikhar@yopmail.com',
                        to: email,
                        subject: "Welcome to Registration",
                        text: text,
                    };

                    transporter.sendMail(info, function (error, info) {
                        if (error) {
                            console.log(error)
                        } else {
                            console.log("Email sent.")
                        }
                    })

                    return await res.status(200).json({
                        "status": "ok",
                        "status code": 200,
                        "response": {
                            Message: `Verification email sent.`,
                        }
                    })
                }
                verified()
            } else if (status == 'blocked') {
                await status_update(status, id)
                return await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": {
                        Message: "Merchant Blocked"
                    }
                })
            }
        } else if (decoded.usertype !== "admin") {
            return await res.status(403).json({
                "status": "ok",
                "status code": 403,
                "response": {
                    Message: `Unauthorized Access!`,
                }
            })
        }
    } catch (err) {
        next(err)
    }
}

