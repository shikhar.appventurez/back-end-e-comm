const { Op } = require('sequelize');
const all_product = require('../../services/common');
const { find_by_id } = require('../../services/merchant-services');
const { merchant} = require('../../authorization/merchantAuthorization')

exports.all_product = async (req, res, next) => {

    try {
        const decoded = merchant(req,res)
        const page = req.query.page ?? 1
        const limit = req.query.limit ?? 10
        var category, subcategory, product = '';

        if (decoded.usertype != "merchant") {

            
            let merchant = { [Op.not]: null }
            req.query.product ? product = { [Op.regexp]: req.query.product } : product = { [Op.not]: null };
            req.query.subcategory ? subcategory = { [Op.regexp]: req.query.subcategory } : subcategory = { [Op.not]: null };
            req.query.category ? category = { [Op.startsWith]: req.query.category } : category = { [Op.not]: null };
            let info = await all_product(category, subcategory, product, merchant, page,limit)
            return res.status(200).send(info)
        } else {

            product = { [Op.not]: null };
            subcategory = { [Op.not]: null };
            category = { [Op.not]: null };
            const data = await find_by_id(decoded.id)
            let merchant = data.fullname

            let info = await all_product(category, subcategory, product, merchant, page,limit)
            res.status(200).send(info)
        }

        
       

    } catch (err) {
        next(err);
    };
};