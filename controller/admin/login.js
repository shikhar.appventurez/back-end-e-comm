const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { user_login } = require('../../services/user-services');
const { merchant_login } = require('../../services/merchant-services');

const userLogin = async (req, res, next) => {

    try {
        const email_or_phone = req.body.email_or_phone
        const row = await user_login(email_or_phone)

        if (row == null) {
            return res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "Incorrect Credentials!" }
            })
        }

        else if (row !== null && row.status !== 'blocked') {

            const thetoken = jwt.sign({ id: row.id, usertype: row.usertype }, 'the-secret-login-code', { expiresIn: '1h' })
            const passmatch = await bcrypt.compare(req.body.password, row.password);

            !passmatch || row === null ? await res.status(401).json({
                "status": "ok",
                "status code": 401,
                "response": { "Message": "Incorrect Credentials!" }
            }) :
                await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": {
                        "Message": "User login successful.",
                        Token: thetoken
                    }
                })
        } else {

            await res.status(403).json({
                "status": "ok",
                "status code": 403,
                "response": { Message: "Credentials Blocked!" }
            }) 
        }
    }
    catch (err) {
        next(err)
    }
}

const merchantLogin = async (req, res, next) => {

    try {
        const email_or_phone = req.body.email_or_phone
        const row = await merchant_login(email_or_phone)

        if (row == null) {
            return res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "Incorrect Credentials!" }
            })
        }

        else if (row !== null && row.status !== 'blocked') {
            
            const thetoken = jwt.sign({ id: row.id, usertype: row.usertype }, 'the-secret-login-code', { expiresIn: '1h' })
            const passmatch = await bcrypt.compare(req.body.password, row.password);

            (!passmatch || row === null) ? await res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "Incorrect Credentials!" }
            }) :
                await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": {
                        "Message": "Merchant login successful.",
                        Token: thetoken
                    }
                })
        } else {

            await res.status(403).json({
                "status": "ok",
                "status code": 403,
                "response": { Message: "Credentials Blocked!" }
            }) 
        }
    }
    catch (err) {
        next(err)
    }
}
module.exports = {userLogin,merchantLogin}

