const User = require('../../models/user');


exports.validateUser = async (req, res, next) => {
    if (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]) {
        return res.status(422).send("Message: Please provide the token.")
    }
    try {
        const theToken = req.headers.authorization.split(' ')[1];
        const decoded = jwt.verify(theToken, 'the-secret-login-code')

        if (decoded.usertype == "admin") {
            const id = req.query.id;
            const status = req.query.status;

            if (status !== "") {
                await User.update({
                    status: status,
                    where: { id: id }
                });

            }
        } else if (decoded.usertype !== "admin") {
            return res.send(`Message: Unauthorized Access!`)
        }
    } catch (err) {
        next(err)
    }
}