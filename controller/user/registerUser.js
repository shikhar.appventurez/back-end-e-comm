const bcrypt = require('bcryptjs');
const { find, create } = require('../../services/user-services');
const { register_user } = require('../../authorization/userAuthorization');

exports.registerUser = async (req, res, next) => {
    try {

        const decoded = await register_user(req, res)

        const info = {
            fullname: req.body.fullname,
            email: req.body.email,
            phoneNo: req.body.phoneNo,
            usertype: 'user',
            password: bcrypt.hashSync(req.body.password, 9),
            step: 1,
            status: "verified"
        }

        const email = req.body.email
        const row  = await find(email);

        row !== null ? res.status(422).json({
            "status": "ok",
            "status code": 422,
            "response": { "Message": " Credentials already in use." }
        }) :
            req.body.email == decoded.email ?
                await create(info) && res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": {
                        "Message": "Registered Successfully!",
                    }
                }) :
                res.status(422).json({
                    "status": "ok",
                    "status code": 422,
                    "response": { "Message": "Incorrect Credentials!" }
                })
    }
    catch (err) {
        next(err)
    }
}