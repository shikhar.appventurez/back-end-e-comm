const { user } = require('../../authorization/userAuthorization');
const { add_to_cart, find_product, add_quantity } = require('../../services/product-services');
const { find_cart_product } = require('../../services/user-services');

exports.addToCart = async (req, res, next) => {

    try {

        const decoded = user(req, res)
        if (decoded.usertype == "user") {

            const product = await find_product(req.body.id);
            const cart_product = await find_cart_product(decoded.id, req.body.id)
            let total_quantity = req.body.quantity
            if (+cart_product != 0) {
                total_quantity = +cart_product.quantity + +req.body.quantity
            } else {
                total_quantity = req.body.quantity ?? 1
            }
            if (!product) {
                return await res.status(400).json({
                    "status": "ok",
                    "status code": 400,
                    "response": {
                        "Message": "Bad request",
                    }
                })
            }
            if (total_quantity > product.stock) {
                return await res.status(406).json({
                    "status": "ok",
                    "status code": 406,
                    "response": {
                        "Message": "Insufficient Stock.",
                    }
                })
            }
            if (product) {
                const quantity = req.body.quantity
                const info = {
                    quantity: quantity,
                    product_id: req.body.id,
                    user_id: decoded.id,
                    totalprice: (product.discounted_price * quantity)

                }
                const [cart_product, created] = await add_to_cart(info, product, decoded.id)
                if (created) {
                    return await res.status(200).json({
                        "status": "ok",
                        "status code": 200,
                        "response": {

                            "Message": "Product added to cart.",
                        }
                    })
                } else {
                    const totalprice = cart_product.totalprice + (quantity * product.discounted_price)
                    const updated_quantity = +req.body.quantity + +cart_product.quantity
                    await add_quantity(updated_quantity, totalprice, cart_product);
                    return await res.status(200).json({
                        "status": "ok",
                        "status code": 200,
                        "response": {

                            "Message": "Product added to cart.",
                        }
                    })
                }

            } else {
                return await res.status(400).json({
                    "status": "ok",
                    "status code": 400,
                    "response": {
                        "Message": "Bad Request!",
                    }
                })
            }

        } else {
            return await res.status(403).json({
                "status": "ok",
                "status code": 403,
                "response": { Message: "Unauthorized Access!" }
            })
        }
    }
    catch (err) {
        next(err)
    }
}