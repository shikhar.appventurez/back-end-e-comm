
const Razorpay = require('razorpay');
const crypto = require('crypto')



const razorpay = async function (req,res) {
    const instance = new Razorpay({
        key_id: process.env.Key_id,
        key_secret: process.env.Key_secret,
    });
    const total_price = 4200
    let customer = await instance.customers.fetch("cust_KH1fLzi9KOmNZU")
    if (customer.isEmpty) {
        customer = await instance.customers.create({
            name: "Shikhar",
            contact: "9876543210",
            email: "shikhar@yopmail.com",
            fail_existing: 0,
            // gstin: "29XAbbA4369J1ZA",

        })

    }
    let options = {
        amount: total_price,
        currency: 'INR',
        receipt: 'rcptid_11',
        customer_id: customer.id,
        payment: {
            capture: 'automatic',
            capture_options: {
                automatic_expiry_period: 12,
                manual_expiry_period: 7200,
                refund_speed: 'optimum'
            }
        }
    }

    await instance.orders.create(options,
        (err, order) => {

            //STEP 3 & 4: 
            if (!err){
                
                res.json({order:order})
                
            }
            
            else
                res.send(err);
        }
    )
}


const razorverify = function(req, res) {
    
    let body = req.body.response.razorpay_order_id + "|" + req.body.response.razorpay_payment_id;
    
    var expectedSignature = crypto.createHmac('sha256', 'XICAwohyF34JhAmv9LByyRi2')
        .update(body.toString())
        .digest('hex');

    var response = { "signatureIsValid": "false" }
    if (expectedSignature === req.body.response.razorpay_signature) {
        response = { "signatureIsValid": "true" }
        res.send(response)
    }
}


module.exports = {
razorpay,
razorverify
}