const { user } = require("../../authorization/userAuthorization");
const { order_list } = require("../../services/common");


exports.ordered_list = async (req, res, next) => {

    try {
        const decoded = user(req, res);
        const id = decoded.id
        if (decoded) {
            const orderList = await order_list(id);
            const total_ordered_products = orderList.length
            return await res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": { Total_ordered_products: total_ordered_products, Ordered_products: orderList }
            })

        } else {
            return await res.status(401).json({
                "status": "ok",
                "status code": 401,
                "response": { Message: "Unauthorized!" }
            })
        }



    } catch (err) {
        next(err);
    };
};