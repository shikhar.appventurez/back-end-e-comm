const { user_details } = require('../../authorization/userAuthorization');
const { find, update, find_by_id } = require('../../services/user-services');

exports.userDetails = async (req, res, next) => {
    try {
        const decoded = await user_details(req, res)
        const info = {
            address: req.body.address,
            step: '0'
        }
        const id = decoded.id
        const row = await find_by_id(id)

        if (row != null && row.step == "1") {
            await update(info, id) && res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": {
                    "Message": "Details Saved!",
                }
            })
        } else {
            res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "Invalid Details!" }
            })
        }
    }
    catch (err) {
        next(err)
    }
}