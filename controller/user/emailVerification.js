const jwt = require('jsonwebtoken');
const { email_verify } = require('../../authorization/userAuthorization');
const { otpVerify } = require('../../services/user-services');


exports.emailVerification = async (req, res, next) => {

    try {

        const decoded = await email_verify(req, res);

        const thetoken = jwt.sign({ email: decoded.email }, 'the-secret-registration-code', { expiresIn: '1h' })
        const row = await otpVerify(decoded)

        req.body.otp == row.otp ? res.status(200).json({
            "status": "ok",
            "status code": 200,
            "response": {
                "Message": "OTP Verified!",
                "Token": thetoken
            }
        }) :
            res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "Incorrect OTP!" }
            })

        return

    }
    catch (err) {
        if (err.message == "jwt expired") {
            return res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "OTP Expired!" }
            })
        }
        next(err)
    }
}