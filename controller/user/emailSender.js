const nodemailer = require("nodemailer");
const otpGenerator = require('otp-generator')
const jwt = require('jsonwebtoken');
const { updateOrCreate, find } = require("../../services/user-services");
const dotenv = require('dotenv').config()

exports.emailsender = async (req, res, next) => {
    try {

        const data = {
            otp: otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false, lowerCaseAlphabets: false }),
            email: req.body.email,
        }
        const text = `Here is your following generated otp: ${data.otp}`;
        const thetoken = jwt.sign({ email: req.body.email }, 'the-secret-code', { expiresIn: '300s' })


        const verified = async function () {
            const transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: { user: 'shikhar@appventurez.com', pass: process.env.pass }

            })

            updateOrCreate(req.body.email, data)


            const info = {
                from: 'shikhar@yopmail.com',
                to: data.email,
                subject: "Welcome to Registration",
                text: text,
            };

            transporter.sendMail(info, async function (error, info) {
                if (error) {
                    console.log(error)
                } else {
                    return await res.status(200).json({
                        "status": "ok",
                        "status code": 200,
                        "response": {
                            Message: `Verification email sent.`,
                            Token: thetoken
                        }
                    })
                }
            })




        }
        const email = req.body.email
        const row = await find(email)
        if (row !== null) {
            await res.status(409).json({ "status": "ok", "status code": 409, "response": { "Message": "User already in use." } })
        } else if (row == null) {
            verified()
        }
    }

    catch (err) {
        next(err)
    }
}
