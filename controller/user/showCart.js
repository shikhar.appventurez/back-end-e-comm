const { user } = require('../../authorization/userAuthorization');
const { cart_product } = require('../../services/product-services');
const { user_cart } = require('../../services/user-services');

exports.showCart = async (req, res, next) => {

    try {

        const decoded = user(req, res)
        if (decoded.usertype == "user") {
            const cart = await user_cart(decoded.id)
            const product = []
            if (cart.length == "0") {
                return await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": { Message: "Cart Empty!" }
                })
            } else {
                let total_price = 0
                for (i in cart) {
                    const show_product = await cart_product(cart[i].product_id)
                    show_product.quantity = cart[i].quantity
                    product.push(show_product);
                    const product_price = show_product.discounted_price * cart[i].quantity
                    total_price += product_price
                }


                if (total_price < 1000) {
                    return await res.status(200).json({
                        "status": "ok",
                        "status code": 200,
                        "response": { Shipping_Charges: "Rs 150", Cart_Total: 'Rs ' + total_price, Products: product }
                    })
                }
                return await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": { Shipping_Charges: "Rs 0", Cart_Total: 'Rs ' + total_price, Products: product }
                })
            }

        }
    }
    catch (err) {
        next(err)
    }
}