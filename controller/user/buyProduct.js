const { user } = require('../../authorization/userAuthorization');
const { order_product, decrease_stock, find_all_product } = require('../../services/product-services');
const { user_cart, find_by_id, order_id, empty_cart, ordered_product, order_update } = require('../../services/user-services');

const dotenv = require('dotenv').config()



exports.buy = async (req, res, next) => {

    try {


        const decoded = user(req, res)
        if (decoded.usertype == "user") {
            const cart = await user_cart(decoded.id)
            const product = []
            const updated_product = []
            let totalprice = 0

            if (cart.length == 0) {
                return await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": { Message: "Cart Empty!" }
                })
            } else {
                let total_price = 0
                const orderid = await order_id(total_price, decoded)
                const productId = []
                const user = await find_by_id(decoded.id)

                for (i in cart) {
                    productId.push(cart[i].product_id)
                }

                const show_product = await order_product(productId)
                for (i in show_product) {
                    if (cart[i].quantity > show_product[i].stock) {
                        return res.status(200).json({
                            "status": "ok",
                            "status code": 200,
                            "response": { Message: "Not in stock" }
                        })
                    }
                    const product_price = show_product[i].discounted_price * cart[i].quantity

                    show_product[i].quantity = cart[i].quantity
                    show_product[i].total_price = product_price
                    show_product[i].user_id = decoded.id
                    show_product[i].product_id = productId[i]
                    show_product[i].order_id = orderid.id
                    show_product[i].address = user.address
                    product.push(show_product[i])
                    total_price += product_price
                }

                for (i in cart) {
                    totalprice = cart[i].totalprice + totalprice
                }
                if (total_price == totalprice) {
                    const find_product = await find_all_product(productId)

                    for (i in product) {
                        const updatedStock = (show_product[i].stock - cart[i].quantity)
                        find_product[i].stock = updatedStock
                        updated_product.push(find_product[i])
                    }

                    const customer = await stripe.customers.create({
                        id: user.id,
                        email: user.email,
                        name: user.fullname,
                        phone: user.phoneNo,
                    })

                    let paymentMethod = await stripe.paymentMethods.create({
                        type: 'card',
                        card: {
                            number: process.env.number,
                            exp_month: process.env.exp_month,
                            exp_year: process.env.exp_year,
                            cvc: process.env.cvc,
                        },
                        billing_details: {
                            name: user.fullname,
                            email: user.email
                        },

                    });

                    paymentIntent = await stripe.paymentIntents.create({
                        payment_method: paymentMethod.id,
                        amount: total_price * 100, // USD*100
                        currency: 'inr',
                        payment_method_types: ['card'],
                        customer: customer.id,
                        confirmation_method: 'automatic',
                        confirm: true,
                        off_session: true
                    })

                    if (paymentIntent.paid) {
                        await decrease_stock(updated_product)
                        await ordered_product(product)
                        await order_update(total_price, orderid)
                        await empty_cart(decoded.id)
                        return await res.status(200).json({
                            "status": "ok",
                            "status code": 200,
                            "response": { Message: "Order Confirmed", Payment: payment }
                        })
                    } else if (!paymentIntent.paid) {
                        return await res.status(400).json({
                            status: "ok",
                            "status code": 400,
                            response: { Message: "Please complete the payment process." }
                        })
                    }

                } else {
                    return await res.status(200).json({
                        "status": "ok",
                        "status code": 200,
                        "response": { Message: "Product prices have been updated!" }
                    })
                }
            }

        } else {
            return await res.status(403).json({
                "status": "ok",
                "status code": 403,
                "response": { Message: "Unauthorized Access!" }
            })
        }
    }
    catch (err) {
        console.log(err)
        next(err)
    }
}