
const dotenv = require('dotenv').config()
const stripe = require("stripe")(process.env.STRIPE_PRIVATE_KEY)
// const elements = stripe.elements();



exports.stripe = function(req, res){
 
    // Moreover you can take more details from user
    // like Address, Name, etc from form
    console.log(req.body.stripeToken)
    stripe.customers.create({
        email: "johndoe@yopmail.com",
        // source: req.body.stripeToken,
        name: 'John doe',
        address: {
            line1: 'Block-123',
            postal_code: '452331',
            city: 'Indore',
            state: 'Madhya Pradesh',
            country: 'India',
        }
    })
   

    .then((customer) => {

        
 
        return stripe.charges.create({
            amount: 2500,     // Charging Rs 25
            description: 'test-product',
            currency: 'INR',
            customer: customer.id,
            // source:""
        });
    })
    .then((charge) => {
        alert("Success")
        console.log(charge)
        res.send("Success")  // If no error occurs
    })
    .catch((err) => {
        console.log(err)
        res.send(err)       // If some error occurs
    });
}