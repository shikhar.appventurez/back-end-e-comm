const { user } = require('../../authorization/userAuthorization');
const { admin_get_user, get_user } = require('../../services/user-services');

exports.getUser = async (req, res, next) => {

    try {
        const decoded = await user(req, res)
        const page = req.query.page ?? 1
        const limit = req.query.limit ?? 10
        if (decoded.usertype == "admin") {

            const row = await admin_get_user(page, limit)

            const info = {
                Total_Users: row.length,
                Users: row
            }

            return res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": info
            })

        } else if (decoded.usertype == "user") {
            const id = decoded.id
            const row = await get_user(id)

            return res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": {
                    Users: row.dataValues
                }
            })

        } else {
            return res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "Incorrect Credentials!" }
            })
        }


    }
    catch (err) {
        next(err)
    }
}