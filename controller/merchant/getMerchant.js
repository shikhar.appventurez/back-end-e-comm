const { merchant } = require('../../authorization/merchantAuthorization');
const { user } = require('../../authorization/userAuthorization');
const { admin_get_merchant, get_merchant } = require('../../services/merchant-services');

exports.getMerchant = async (req, res, next) => {

    try {
        const decoded = merchant(req, res)
        const page = req.query.page ?? 1
        const limit = req.query.limit ?? 10
        const status = req.query.status ?? "verified"
        if (decoded.usertype == "merchant") {
            const row = get_merchant(decoded.id)

            return res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": {
                    Merchant: row.dataValues
                }
            })
        } else if (decoded.usertype == "admin") {
            const row = await admin_get_merchant(page, limit, status)
            const info = {
                Total: row.length,
                Merchants: row
            }
            return res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": info
            })
        } else {
            return res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": "Incorrect Credentials!" }
            })
        }



    } catch (err) {
        next(err)
    }
}