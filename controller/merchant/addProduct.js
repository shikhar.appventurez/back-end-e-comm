const { merchant } = require('../../authorization/merchantAuthorization');
const { add_category, add_sub_category, add_product } = require('../../services/product-services');

exports.addProduct = async (req, res, next) => {

    try {
        const decoded = merchant(req, res)

        if (decoded.usertype == 'merchant') {

            const [category] = await add_category(req.body.category)
            const [subcategory] = await add_sub_category(req.body.subcategory, category)
            const discounted_price = +req.body.price * ((100 - (req.body.discount ?? 0)) / 100)

            const info = {
                product_name: req.body.product_name,
                stock: req.body.stock,
                price: req.body.price,
                discount: req.body.discount ?? 0,
                merchant_id: decoded.id,
                subcategory_id: subcategory.id,
                Category_ID: category.id,
                discounted_price: discounted_price
            }
            const [existing, created] = await add_product(info, req.body.product_name, category, subcategory, decoded.id)
            if (!created) {
                return await res.status(422).json({
                    "status": "ok",
                    "status code": 422,
                    "response": {
                        "Message": "Product already exists.",
                    }
                })
            } else {
                return await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": {
                        "Message": "Product Added.",
                    }
                })
            }


        } else {
            return await res.status(403).json({
                "status": "ok",
                "status code": 403,
                "response": { Message: "Unauthorized Access!" }
            })
        }
    }
    catch (err) {
        next(err)
    }
}