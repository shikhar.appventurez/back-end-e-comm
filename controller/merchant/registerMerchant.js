const { find, update_or_create } = require('../../services/merchant-services')

exports.registerMerchant = async (req, res, next) => {
    try {

        const info = {
            fullname: req.body.fullname,
            email: req.body.email,
            phoneNo: req.body.phoneNo,
            usertype: 'merchant',
            address: req.body.address
        }


        const email = req.body.email
        const row = await find(email);
        row !== null && row.status == "verified" ?
            await res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": " Credentials already in use." }
            }) :
            await update_or_create(email, info) &&
            await res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": {
                    "Message": "Details forwarded for verification.",
                }
            })
    }
    catch (err) {
        next(err)
    }
}