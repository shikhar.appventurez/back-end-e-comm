const bcrypt = require('bcryptjs');
const { verification } = require('../../authorization/merchantAuthorization');
const { find_by_id, update_by_id } = require('../../services/merchant-services');

exports.merchantVerification = async (req, res, next) => {
    try {
        const decoded = verification(req, res )
        const row = find_by_id(decoded.id)

        if (row.password == null) {
            const info = {
                password: bcrypt.hashSync(req.body.password, 10)
            }

            await update_by_id(info, decoded.id)
            return await res.status(200).json({
                "status": "ok",
                "status code": 200,
                "response": {
                    "Message": "Merchant Successfully Created.",
                }
            })
        }
        else {
            return await res.status(422).json({
                "status": "ok",
                "status code": 422,
                "response": { "Message": " Credentials already in use." }
            })
        }
    }
    catch (err) {
        next(err)
    }
}