const { merchant } = require('../../authorization/merchantAuthorization');
const { find_product, add_stock } = require('../../services/product-services');


exports.addStock = async (req, res, next) => {
    try {
        const decoded = merchant(req, res);

        if (decoded.usertype == 'merchant') {
            const data = await find_product(req.body.id)
            const discounted_price = (req.body.price ?? data.price) * ((100 - (req.body.discount ?? 0)) / 100)
            if (data.merchant_id == decoded.id) {
                const info = {
                    stock: +data.stock + +req.body.stock,
                    price: req.body.price,
                    discount: req.body.discount,
                    discounted_price: discounted_price
                }
                await add_stock(info, req.body.id)
                return await res.status(200).json({
                    "status": "ok",
                    "status code": 200,
                    "response": {
                        "Message": "Details updated.",
                    }
                })

            } else {
                await res.status(403).json({
                    "status": "ok",
                    "status code": 403,
                    "response": { "Message": "Unauthorized! Details mismatch." }
                })
            }

        } else {
            return await res.status(403).json({
                "status": "ok",
                "status code": 403,
                "response": { Message: "Unauthorized Access!" }
            })
        }
    }
    catch (err) {
        next(err)
    }
}